﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace HW2_Calc
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Handle_7Clicked (object sender, EventArgs e)
        {
            Display.Text += "7";
        }

        private void Handle_8Clicked (object sender, EventArgs e)
        {
            Display.Text += "8";
        }

        private void Handle_9Clicked (object sender, EventArgs e)
        {
            Display.Text += "9";
        }

        private void Handle_4Clicked (object sender, EventArgs e)
        {
            Display.Text += "4";
        }

        private void Handle_5Clicked (object sender, EventArgs e)
        {
            Display.Text += "5";
        }

        private void Handle_6Clicked (object sender, EventArgs e)
        {
            Display.Text += "6";
        }

        private void Handle_1Clicked (object sender, EventArgs e)
        {
            Display.Text += "1";
        }

        private void Handle_2Clicked (object sender, EventArgs e)
        {
            Display.Text += "2";
        }

        private void Handle_3Clicked (object sender, EventArgs e)
        {
            Display.Text += "3";
        }

        private void Handle_0Clicked (object sender, EventArgs e)
        {
            Display.Text += "0";
        }
    
        private void Handle_clrClicked (object sender, EventArgs e)
        {
            Display.Text = "";
        }
    }
}
